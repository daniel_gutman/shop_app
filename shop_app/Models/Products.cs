using System;

namespace shop_app.Models
{
    public class Product
    {
        public int ID { get; set; }
        public int product_id { get; set; }
        public double product_price { get; set; }
        public string product_name { get; set; }
        public int product_count { get; set; }
    }

}