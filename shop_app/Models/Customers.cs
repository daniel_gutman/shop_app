using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace MvcCustomer.Models

{
    public class Customer
    {
        public int ID { get; set; }
        public int customer_id { get; set; }
        public string customer_name { get; set; }
        public string customer_addresses { get; set; }
    }

}