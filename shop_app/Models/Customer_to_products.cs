using System;

namespace shop_app.Models
{
    public class Customer_to_product
    {
        public int ID { get; set; }
        public int customer_id { get; set; }
        public int product_id { get; set; }
    }

}