using System;

namespace shop_app.Models
{
    public class Supply_to_product
    {
        public int ID { get; set; }
        public int supply_id { get; set; }
        public int product_id { get; set; }
    }

}